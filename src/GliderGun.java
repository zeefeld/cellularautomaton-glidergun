/**
 * A  class that simulates a Turing complete cellular automaton. 
 *
 * @author zeefeld
 */
public class GliderGun
{
	private boolean[][] cells;
	
	/**
	 * Constructor that initializes a 2 dimensional boolean array.
	 *
	 * @param cells
	 */
	public GliderGun(boolean[][] cells) 
	{
		this.cells = cells;
	}
	
	/**
	 * Method to verify that a cell lies within the boundary of the boolean 
	 * array.
	 *
	 * @param cells boolean array
	 * @param i row index of cells
	 * @param j column index of cells
	 * @return true if index is in bounds and false if  index is out of bounds
	 */
	public boolean inBounds(boolean[][] cells, int i, int j)
	{
		if (i >= 0 && i < cells.length && j >= 0 && j < cells[0].length)
		{
			return true; //index is in bounds
		} 
		else 
		{
			return false; //index is out of bounds
		}
	}

	/**
	 * Method that counts loops through the 2 dimensional array and counts the
	 * adjacent cells that are true.
	 *
	 * @param cells
	 * @param i row index of cells
	 * @param j column index of cells
	 * @return count the integer value of adjacent cells
	 */
	public int countAdjacentCells(boolean[][] cells, int row, int column)
	{
		int count = 0;
		if (cells[row][column])
		{
			count = -1; //if cell is true do not count it
		} 
		else 
		{
			count = 0; //if cell is false do not count it
		}
		//loop through adjacent cells within the row
		for (int i = row - 1; i <= row + 1; i++)
		{
			//loop through adjacent cells in the columns
			for (int j = column - 1; j <= column + 1; j++)
			{
				//if adjacent cells are in bounds and the cell is true 
				//increment the count
				if (inBounds(cells, i, j) && cells[i][j])
				{
					count++;
				}
			}
		}
		return count;
	}
	
	/**
	 * Method that finds the current state of a cell. If the state of a cell is 
	 * true and it has 2 or 3 adjacent cells, the state of the cell remains 
	 * true. If the state of the cell is false and the cell has 3 adjacent 
	 * cells, the state of the cell becomes true. For all other conditions the
	 * state of the cell becomes or remains false.
	 *
	 * @param state
	 * @param numberOfAdjacentCells
	 * @return true if location in array should be true
	 * 		   false if location in array should be false	
	 */
	public boolean currentState(boolean state, int numberOfAdjacentCells)
	{ 
		//if state is true and has 2 or 3 adjacent cells, then true
		if (state && (numberOfAdjacentCells == 2 || numberOfAdjacentCells == 3))
		{
			return true;
		}
		//if state is false and there are 3 adjacent cells then true
		else if (!state && numberOfAdjacentCells == 3)
		{
			return true;
		}
		//all other conditions are false
		else
		{
			return false;
		}
	}
	
	/**
	 * Method that calculates the next generation of cells.
	 */
	public void calculateNextGeneration() 
	{
		//create an array with the same dimensions as cells
		boolean[][] nextGeneration = new boolean[cells.length][cells[0].length];
		int numberOfAdjacentCells = 0;
		//loop through the row indices
		for (int i = 0; i < cells.length; i++)
		{
			//loop through the column indices
			for (int j = 0; j < cells[0].length; j++)
			{ 
				//get number of adjacent cells
				numberOfAdjacentCells = countAdjacentCells(cells, i, j);
				//determine the state of the cell
				if (currentState(cells[i][j], numberOfAdjacentCells))
				{
					nextGeneration[i][j] = true;
				}
				else
				{
					nextGeneration[i][j] = false;
				}
			}
		}
		
		cells = nextGeneration;
	}
	
	/** (non-Javadoc)
	 * Override built in toString() to display boolean array.
	 * @return stringRepresentation a String object which acts as the visual 
	 * 			representation of the boolean array
	 */
	public String toString()
	{
		String stringRepresentation = "";
		//loop through rows of array
		for (int i = 0; i < cells.length; i++)
		{
			stringRepresentation = stringRepresentation + "|";
			//loop through columns of array
			for (int j = 0; j < cells[0].length; j++)
			{
				if (cells[i][j] == true)
				{
					//display a cell
					stringRepresentation = stringRepresentation + "*";
				}
				else 
				{
					//do not display cell
					stringRepresentation = stringRepresentation + " ";
				}
			}
			stringRepresentation = stringRepresentation + "|\n";
		}
		return stringRepresentation;
	}

}
