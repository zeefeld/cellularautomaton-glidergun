import java.io.PrintStream;


/**
 * Class to test the LifeRunner class.  
 *
 * @author zeefeld
 */
public class GliderGunTester 
{
	/**
	 * 
	 * @param args Command Line arguments -- not used here
	 */
	public static void main(String[] args) 
	{
		PrintStream out  = System.out;
		final int NUM_GENERATIONS = 500;
		boolean[][] cells = new boolean[60][60];
		int s = 2;
		int d = 3;

		cells[s     ][d +  2] = true;
		cells[s     ][d +  3] = true;
		cells[s +  1][d +  2] = true;
		cells[s +  1][d +  3] = true;
		cells[s +  8][d +  3] = true;
		cells[s +  8][d +  4] = true;
		cells[s +  9][d +  2] = true;
		cells[s +  9][d +  4] = true;
		cells[s + 10][d +  2] = true;
		cells[s + 10][d +  3] = true;
		cells[s + 16][d +  4] = true;
		cells[s + 16][d +  5] = true;
		cells[s + 16][d +  6] = true;
		cells[s + 17][d +  4] = true;
		cells[s + 18][d +  5] = true;
		cells[s + 22][d +  1] = true;
		cells[s + 22][d +  2] = true;
		cells[s + 23][d +  0] = true;
		cells[s + 23][d +  2] = true;
		cells[s + 24][d +  0] = true;
		cells[s + 24][d +  1] = true;
		cells[s + 24][d + 12] = true;
		cells[s + 24][d + 13] = true;
		cells[s + 25][d + 12] = true;
		cells[s + 25][d + 14] = true;
		cells[s + 26][d + 12] = true;
		cells[s + 34][d     ] = true;
		cells[s + 34][d +  1] = true;
		cells[s + 35][d     ] = true;
		cells[s + 35][d +  1] = true;
		cells[s + 35][d +  7] = true;
		cells[s + 35][d +  8] = true;
		cells[s + 35][d +  9] = true;
		cells[s + 36][d +  7] = true;
		cells[s + 37][d +  8] = true;
		
		
		GliderGun l = new GliderGun(cells);
		
		for (int generation=0; generation < NUM_GENERATIONS; generation++)
		{
			l.calculateNextGeneration();
			out.println(l);
			try
			{ 
				Thread.sleep(500); //delay .5 seconds per loop iteration
			} 
			catch(InterruptedException ie)
			{ 
				
			} 
		}
	}
}


